﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        //if (other.CompareTag("Obstacle") || other.CompareTag("Boost"))
        //{
        //    Destroy(other.gameObject);
        //}
        if (other.CompareTag("Enemy"))
        {
            Destroy(other.gameObject);
        }
        //else
        //{
        //    Destroy(other.gameObject, 3);
        //}
    }
}
