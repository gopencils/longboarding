﻿using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using static UnityEngine.ParticleSystem;

public class PlayerMovement : MonoBehaviour
{

    public Touch initTouch = new Touch();
    public Vector3 originPos;
    public float positionX, positionY;
    public float speed = 0.5f, computerSpeed/*, dir = -1f*/;
    public float rotationSpeed;
    public float mapWidth = 2f;
    public bool touching = false;
    bool isSlowMode = false;
    float h, v;
    Vector3 dir;
    SplineFollower follow;
    public Transform ghost;
    Vector3 lastDir;
    float restoreRotation;
    float restorePosition;
    public ParticleSystem effect;

    private Rigidbody rb;
    Tweener tween1;
    Tweener tween2;

    void Start()
    {
        //Initializations
        rb = GetComponent<Rigidbody>();
        positionX = 0f;
        positionY = transform.localPosition.y;
        originPos = transform.localPosition;
        follow = GetComponent<SplineFollower>();
        restorePosition = 150;
        restoreRotation = 60;
    }

    //private void Update()
    //{
    //    if (!isSlowMode)
    //    {
    //        Camera.main.transform.parent.position = Vector3.Lerp(Camera.main.transform.parent.position, transform.position, Mathf.Abs(restorePosition - 50) * Time.deltaTime);
    //        Camera.main.transform.parent.rotation = Quaternion.Lerp(Camera.main.transform.parent.rotation, transform.rotation, Mathf.Abs(restoreRotation - 30) * Time.deltaTime);
    //    }
    //    else
    //    {
    //        Camera.main.transform.parent.position = Vector3.Lerp(Camera.main.transform.parent.position, transform.position, 15 * Time.deltaTime);
    //        Camera.main.transform.parent.rotation = Quaternion.Lerp(Camera.main.transform.parent.rotation, transform.rotation, 6 * Time.deltaTime);
    //    }
    //}

    void FixedUpdate()
    {
        //foreach (Touch touch in Input.touches)
        //{
        //    if (touch.phase == TouchPhase.Began)        //if finger touches the screen
        //    {
        //        if (touching == false)
        //        {
        //            touching = true;
        //            initTouch = touch;
        //        }
        //    }
        //    else if (touch.phase == TouchPhase.Moved)       //if finger moves while touching the screen
        //    {
        //        float deltaX = initTouch.position.x - touch.position.x;
        //        positionX -= deltaX * Time.deltaTime * speed * dir;
        //        positionX = Mathf.Clamp(positionX, -mapWidth, mapWidth);      //to set the boundaries of the player's position
        //        transform.localPosition = new Vector3(positionX, positionY, 0f);
        //        initTouch = touch;
        //    }
        //    else if (touch.phase == TouchPhase.Ended)       //if finger releases the screen
        //    {
        //        initTouch = new Touch();
        //        touching = false;
        //    }
        //}

        //if you play on computer---------------------------------
        //float x = Input.GetAxis("Horizontal") * Time.deltaTime * computerSpeed;     //you can move by pressing 'a' - 'd' or the arrow keys
        ////Vector3 newPosition = rb.transform.localPosition + Vector3.right * x;
        //////newPosition.x = Mathf.Clamp(newPosition.x, -mapWidth, mapWidth);
        ////transform.localPosition = newPosition;
        ////--------------------------------------------------------
        //Debug.Log(x);
        //GetComponent<SplineFollower>().motion.offset = new Vector2(x, 0);
        Camera.main.transform.parent.position = Vector3.Lerp(Camera.main.transform.parent.position, transform.position, 30 * Time.deltaTime);
        Camera.main.transform.parent.rotation = Quaternion.Lerp(Camera.main.transform.parent.rotation, transform.rotation, 2 * Time.deltaTime);
        rb.velocity = Vector3.Lerp(rb.velocity, lastDir * 50, 3 * Time.deltaTime);
        lastDir = transform.forward;
        //if (Input.GetMouseButtonDown(0))
        //{
        //    isSlowMode = false;
        //}
        //if (!isSlowMode)
        //{
            if(dir!=Vector3.zero)
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), 1.5f * Time.deltaTime);
            if (Input.GetMouseButton(0))
            {
                OnMouseDrag();
            }
            //if(Input.GetMouseButtonUp(0))
            //{
            //    isSlowMode = true;
            //}
        //}
        //else
        //{
        //    //transform.Rotate(0, 2, 0);
        //}
    }

    void OnMouseDrag()
    {
#if UNITY_EDITOR
        h = Input.GetAxis("Mouse X");
        v = Input.GetAxis("Mouse Y");
#endif
#if UNITY_IOS
        if (Input.touchCount > 0)
        {
            h = Input.touches[0].deltaPosition.x / 10;
            v = Input.touches[0].deltaPosition.y / 10;
        }
#endif
        dir = new Vector3(h, 0, v);
        //if (dir != Vector3.zero)
        //ghost.GetComponent<SplineFollower>().motion.offset = new Vector2(Mathf.Clamp(ghost.GetComponent<SplineFollower>().motion.offset.x + (dir.x * 2), -18, 18), 0);
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if(other.CompareTag("Stunt"))
    //    {
    //        isSlowMode = true;
    //        follow.follow = false;
    //        rb.AddForce(transform.forward * 10000);
    //        Time.timeScale = 0.5f;
    //        Time.fixedDeltaTime = 0.02F * Time.timeScale;
    //    }
    //}
}