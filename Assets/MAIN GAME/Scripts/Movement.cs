﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using MoreMountains.NiceVibrations;
using Dreamteck.Splines;
using UnityEngine.EventSystems;

public class Movement : MonoBehaviour
{
    Rigidbody rigid;
    float h, v;
    Vector3 dir;
    public float speed;
    float originSpeed;
    bool isGround = true;
    public GameObject destroyParticle;
    public GameObject trail;
    RaycastHit hit;
    Coroutine botAI;
    public bool isControl = false;
    public bool isStartGame = false;
    public bool isDrag = false;
    public Animator anim;
    public GameObject skateBoard;
    public GameObject skateBoardFinish;
    bool isFinish = false;
    float bonusForce;
    public GameObject powerBar;
    bool isKick = true;
    public List<Mesh> listChar = new List<Mesh>();
    public SkinnedMeshRenderer skinChar;
    public SkinnedMeshRenderer skinSkateboard;
    Vector3 rotationAmount;
    public Quaternion lastQua = Quaternion.Euler(0,0,0);
    public Vector3 lastRot = Vector3.zero;
    public ParticleSystem smokeEffect;
    float cameraSpeed = 12;
    float cameraRot = 1.5f;
    bool isStunt = false;
    public LayerMask turnMask;
    public LayerMask groundMask;
    bool isUnsteady = false;
    Transform target;
    Transform currentTurn;
    bool isPlayAnim = false;
    public GameObject wheelHub;

    // Start is called before the first frame update
    void OnEnable()
    {
        isStartGame = true;
        isControl = true;
        rigid = GetComponent<Rigidbody>();
        lastRot = transform.eulerAngles;
        bonusForce = 1;
        originSpeed = speed;
        if(CompareTag("Enemy"))
        {
            var random = Random.Range(0, listChar.Count);
            skinChar.sharedMesh = listChar[random];
            botAI = StartCoroutine(botDecision());
        }
        else
        {
            anim.Play("Start");
        }
    }

    void FixedUpdate()
    {
        if (CompareTag("Player"))
        {
            //Camera.main.transform.parent.position = Vector3.Lerp(Camera.main.transform.parent.position, transform.position, cameraSpeed * Time.deltaTime);
            //Camera.main.transform.parent.rotation = Quaternion.Lerp(Camera.main.transform.parent.rotation, transform.rotation, cameraRot * Time.deltaTime);
            //transform.rotation = Quaternion.Euler(ClampAngle(transform.eulerAngles.x, -15, 15), ClampAngle(transform.eulerAngles.y, lastRot.y - 15, lastRot.y + 15), 0);
            //transform.GetChild(0).transform.rotation = Quaternion.Lerp(transform.GetChild(0).transform.rotation, Quaternion.Euler(0,0,0), 5 * Time.deltaTime);

            //if (Physics.Raycast(transform.position, Vector3.down, out hit, 1.5f, groundMask))
            //{
            //    if (hit.transform.CompareTag("Road"))
            //    {
            //        isGround = true;
            //        Debug.Log("true");
            //    }
            //}
            //else
            //{
            //    isGround = false;
            //    Debug.Log("false");
            //}
            if (Input.GetMouseButton(0))
            {
                OnMouseDrag();
            }
        }
        else
        {
            if(isStartGame)
            BotMovement();
        }
    }
    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < 90 || angle > 270)
        {       // if angle in the critic region...
            if (angle > 180) angle -= 360;  // convert all angles to -180..+180
            if (max > 180) max -= 360;
            if (min > 180) min -= 360;
        }
        angle = Mathf.Clamp(angle, min, max);
        if (angle < 0) angle += 360;  // if angle negative, convert to 0..360
        return angle;
    }

    Coroutine move;
    bool isRunning = false;
    void OnMouseDrag()
    {
        if (CompareTag("Player") && isControl && isStartGame)
        {
#if UNITY_EDITOR
            h = Input.GetAxis("Mouse X");
            v = Input.GetAxis("Mouse Y");
#endif
#if UNITY_IOS
            if (Input.touchCount > 0)
            {
                h = Input.touches[0].deltaPosition.x / 11;
                v = Input.touches[0].deltaPosition.y / 11;
            }
#endif
            isDrag = true;
            dir = new Vector3(h, 0, v);
            //dir = new Vector3(h, 0, Mathf.Clamp(Mathf.Abs(v), 0.1f, 0.5f));
            //dir = Quaternion.Euler(0, transform.eulerAngles.y, 0) * dir;
            //transform.rotation = Quaternion.Euler(transform.eulerAngles.x, ClampAngle(transform.eulerAngles.y, lastRot.y - 15, lastRot.y + 15), 0);
            //transform.GetChild(0).transform.rotation = Quaternion.Lerp(transform.GetChild(0).transform.rotation, Quaternion.LookRotation(dir), 2 * Time.deltaTime);
            //if (dir != Vector3.zero)
            //    GetComponent<SplineFollower>().motion.offset = new Vector2(Mathf.Clamp(GetComponent<SplineFollower>().motion.offset.x + (dir.x * 2), -8, 8), 0);
            //transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x + h, transform.position.y, transform.position.z), 5 * Time.deltaTime);
            if(v > 0.1f && !isRunning)
            {
                move = StartCoroutine(C_Stunt());
            }
            else if(v < -0.1f)
            {
                StopCoroutine(move);
                isRunning = false;
                DOTween.To(() => transform.GetChild(0).transform.localRotation, x => transform.GetChild(0).transform.localRotation = x, new Vector3(0, 0, 0), 0.4f).SetEase(Ease.InOutSine);
                DOTween.To(() => GetComponent<SplineFollower>().motion.offset, x => GetComponent<SplineFollower>().motion.offset = x, new Vector2(0, 0), 0.2f).SetEase(Ease.Linear);
                anim.CrossFade("Idle", 0.15f);
            }
            Debug.Log(h);
            //if (h > 0 && (!anim.GetCurrentAnimatorStateInfo(0).IsName("IdleR") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Ending") && !anim.GetCurrentAnimatorStateInfo(0).IsName("AttackL") && !anim.GetCurrentAnimatorStateInfo(0).IsName("AttackR")) && !isPlayAnim)
            //{
            //    anim.CrossFade("IdleR", 0.05f);
            //}
            //else if (h < 0 && (!anim.GetCurrentAnimatorStateInfo(0).IsName("IdleL") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Ending") && !anim.GetCurrentAnimatorStateInfo(0).IsName("AttackL") && !anim.GetCurrentAnimatorStateInfo(0).IsName("AttackR")) && !isPlayAnim)
            //{
            //    anim.CrossFade("IdleL", 0.05f);
            //}
        }
    }

    IEnumerator C_Stunt()
    {
        isRunning = true;
        DOTween.To(() => GetComponent<SplineFollower>().motion.offset, x => GetComponent<SplineFollower>().motion.offset = x, new Vector2(4, 0), 0.4f).SetEase(Ease.InOutSine);
        DOTween.To(() => transform.GetChild(0).transform.localRotation, x => transform.GetChild(0).transform.localRotation = x, new Vector3(0, 20f, 0), 0.25f).SetEase(Ease.Linear);
        anim.CrossFade("IdleR", 0.15f);
        yield return new WaitForSeconds(0.25f);
        DOTween.To(() => transform.GetChild(0).transform.localRotation, x => transform.GetChild(0).transform.localRotation = x, new Vector3(0, 0, 0), 0.25f);
        yield return new WaitForSeconds(0.25f);
        DOTween.To(() => GetComponent<SplineFollower>().motion.offset, x => GetComponent<SplineFollower>().motion.offset = x, new Vector2(-4, 0), 0.4f).SetEase(Ease.InOutSine);
        DOTween.To(() => transform.GetChild(0).transform.localRotation, x => transform.GetChild(0).transform.localRotation = x, new Vector3(0, -20f, 0), 0.25f).SetEase(Ease.Linear);
        anim.CrossFade("RushL1", 0.15f);
        yield return new WaitForSeconds(0.25f);
        DOTween.To(() => transform.GetChild(0).transform.localRotation, x => transform.GetChild(0).transform.localRotation = x, new Vector3(0, 0, 0), 0.25f);
        yield return new WaitForSeconds(0.25f);
        isRunning = false;
        move = StartCoroutine(C_Stunt());
    }

    float botH;
    void BotMovement()
    {
        if (rigid.velocity.magnitude < speed && isStartGame && !isFinish)
        {
            rigid.velocity = Vector3.Lerp(rigid.velocity, transform.forward * speed, 5 * Time.deltaTime);
            if (!isGround)
                rigid.AddForce(-transform.up * 2000);
        }
        transform.rotation = Quaternion.Euler(ClampAngle(transform.eulerAngles.x, -15, 15), ClampAngle(transform.eulerAngles.y, lastRot.y - 15, lastRot.y + 15), 0);
        rigid.angularVelocity = Vector3.zero;

        if (Physics.Raycast(transform.position, Vector3.down, out hit, 1.5f, groundMask))
        {
            if (hit.transform.CompareTag("Road"))
            {
                isGround = true;
                Debug.Log("true");
            }
        }
        else
        {
            isGround = false;
            Debug.Log("false");
        }

        dir = new Vector3(botH, 0, 0);
        dir = Quaternion.Euler(0, transform.eulerAngles.y, 0) * dir;
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, ClampAngle(transform.eulerAngles.y, lastRot.y - 15, lastRot.y + 15), 0);
        if (dir != Vector3.zero)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), 2 * Time.deltaTime);
        }
        if (h > 0 && (!anim.GetCurrentAnimatorStateInfo(0).IsName("IdleR") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Ending") && !anim.GetCurrentAnimatorStateInfo(0).IsName("AttackL") && !anim.GetCurrentAnimatorStateInfo(0).IsName("AttackR")))
        {
            anim.CrossFade("IdleR", 0.05f);
        }
        else if (h < 0 && (!anim.GetCurrentAnimatorStateInfo(0).IsName("IdleL") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Ending") && !anim.GetCurrentAnimatorStateInfo(0).IsName("AttackL") && !anim.GetCurrentAnimatorStateInfo(0).IsName("AttackR")))
        {
            anim.CrossFade("IdleL", 0.05f);
        }
    }

    IEnumerator botDecision()
    {
        yield return new WaitForSeconds(Random.Range(2f, 3f));
        botH = Random.Range(-1f, 1f);
        speed -= 1;
        originSpeed = speed;
        yield return new WaitForSeconds(Random.Range(0.2f, 0.5f));
        botH = 0;
        botAI = StartCoroutine(botDecision());
    }

    IEnumerator botTurn(GameObject other)
    {
        while(Vector3.Distance(transform.position, other.transform.position) > 12)
        {
            yield return null;
        }
        var time = 0.4f;
        float angle = Quaternion.Angle(lastQua, other.transform.rotation);
        if (angle > 90)
        {
            time = angle / 90 * 0.4f + 0.4f;
        }
        var random = Random.Range(1, 2);
        if (other.transform.eulerAngles.y > transform.eulerAngles.y)
        {
            anim.CrossFade("RushR" + random.ToString(), 0.05f);
        }
        else
        {
            anim.CrossFade("RushL" + random.ToString(), 0.05f);
        }
        transform.DOKill();
        transform.DORotate(lastRot, 0.2f).OnComplete(() =>
        {
            transform.DORotate(rotationAmount, time);
        });
        lastRot = rotationAmount;
        lastQua = other.transform.rotation;
        skateBoard.transform.DOKill();
        var rotMod = rotationAmount;
        if (rotMod.y == 0)
        {
            rotMod = new Vector3(rotMod.x, -90, rotMod.z);
        }
        skateBoard.transform.DOLocalRotate(rotMod, time / 2).OnComplete(() =>
        {
            skateBoard.transform.DOLocalRotate(Vector3.zero, time / 1.5f);
        });
        //smokeEffect.Play();
        //smokeEffect.GetComponentInChildren<ParticleSystem>().Play();
        //trail.SetActive(true);
    }

    IEnumerator Finish()
    {
        cameraSpeed = 30;
        cameraRot = 2;
        isFinish = true;
        isControl = false;
        Time.timeScale = 0.75f;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
        int randomSkill = Random.Range(1, 2);
        isPlayAnim = true;
        anim.CrossFade("Ending" + randomSkill.ToString(), 0.05f);
        transform.DORotateQuaternion(lastQua, 1.2f);
        //DOTween.To(() => Camera.main.fieldOfView, x => Camera.main.fieldOfView = x, 90, 1.5f);
        yield return new WaitForSeconds(1.2f);
        Camera.main.transform.DOLocalMoveZ(0, 0.5f);
        Camera.main.transform.DOLocalMoveY(3.5f, 0.5f);
        transform.DORotateQuaternion(lastQua, 0.1f);
        var oldForce = rigid.velocity;
        rigid.velocity = Vector3.zero;
        rigid.isKinematic = true;
        powerBar.SetActive(false);
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
        //yield return new WaitForSeconds(0.2f);
        powerBar.SetActive(true);
        isKick = false;
        anim.speed = 0.02f;
        cameraSpeed = 0;
        while (!isKick && anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.89f)
        {
            yield return null;
        }
        Time.timeScale = 1f;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
        rigid.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        yield return new WaitForSeconds(0.1f);
        //isFinish = false;
        isControl = true;
        //DOTween.To(() => speed, x => speed = x, 0, 5);
        powerBar.SetActive(false);
        anim.speed = 1;
        yield return new WaitForSeconds(0.5f);
        Camera.main.transform.DOLocalMoveY(0, 0.5f);
        rigid.isKinematic = false;
        //rigid.mass = 1;
        float force = (50000 + (bonusForce) - (Mathf.Abs(powerBar.transform.GetChild(0).transform.GetChild(0).transform.localEulerAngles.z)));
        rigid.AddForce(transform.forward * force);
        anim.transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(false);
        anim.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(false);
        skateBoardFinish.SetActive(true);
        target = skateBoard.transform;
        Camera.main.transform.DOLocalMoveZ(-2, 1);
        DOTween.To(() => cameraSpeed, x => cameraSpeed = x, 30, 2f);
        //skateBoard.transform.DOLocalMoveZ(0, 0.1f);
        //DOTween.To(() => rigid.velocity, x => rigid.velocity = x, new Vector3(rigid.velocity.x, -1, 0), 5f);
        yield return new WaitForSeconds(1);
        rigid.drag = 0.5f;
        rigid.collisionDetectionMode = CollisionDetectionMode.Discrete;
        //rigid.AddForce(-transform.forward * force/2);
        while(rigid.velocity != Vector3.zero)
        {
            yield return null;
        }
        Debug.Log("Stop!");
        isFinish = true;
        isStartGame = false;
        cameraRot = 0;
        Camera.main.transform.DOLocalMove(new Vector3(0, 7, -7.5f), 2);
        Camera.main.transform.DOLocalRotateQuaternion(Quaternion.Euler(45, 0, 0), 2);
    }

    IEnumerator delayAnim(float time)
    {
        isPlayAnim = true;
        yield return new WaitForSeconds(time);
        isPlayAnim = false;
    }

    public void PlayReset()
    {
        StartCoroutine(delayReset());
    }

    IEnumerator delayReset()
    {
        wheelHub.SetActive(false);
        if (CompareTag("Player"))
        {
            cameraRot = 0;
            //DOTween.To(() => Camera.main.fieldOfView, x => Camera.main.fieldOfView = x, 90, 1);
        }
        smokeEffect.Stop();
        trail.SetActive(false);
        if (CompareTag("Player"))
        {
            yield return new WaitForSeconds(1);
            //SceneManager.LoadScene(0);
            isStunt = false;
            //DOTween.To(() => Camera.main.fieldOfView, x => Camera.main.fieldOfView = x, 60, 1);
            wheelHub.SetActive(true);
            GetComponent<BoxCollider>().isTrigger = false;
            cameraSpeed = 30;
            cameraRot = 1.5f;
            rigid.isKinematic = false;
            Time.timeScale = 1f;
            Time.fixedDeltaTime = 0.02F;
            anim.Play("Start");
            if (currentTurn != null)
            {
                if (currentTurn.transform.eulerAngles.y == 90)
                {
                    transform.position = new Vector3(currentTurn.transform.position.x, currentTurn.transform.position.y + 5, currentTurn.transform.position.z - 100);
                    lastRot = new Vector3(0, 0, 0);
                    lastQua.eulerAngles = lastRot;
                    Camera.main.transform.localEulerAngles = lastRot;
                    isControl = true;
                    isStartGame = true;
                }
                else if (currentTurn.transform.eulerAngles.y == 0)
                {
                    transform.position = new Vector3(currentTurn.transform.position.x - 100, currentTurn.transform.position.y + 5, currentTurn.transform.position.z);
                    lastRot = new Vector3(0, 90, 0);
                    lastQua.eulerAngles = lastRot;
                    Camera.main.transform.localEulerAngles = lastRot;
                    isControl = true;
                    isStartGame = true;
                }
            }
        }
        else
        {
            Debug.LogError("1");
            isStartGame = false;
            tag = "Slip";
            while (anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.89f)
            {
                yield return null;
            }
            Debug.LogError("2");
            DOTween.To(() => rigid.velocity, x => rigid.velocity = x, Vector3.zero, 0.5f);
            anim.speed = 0;
            //Destroy(gameObject);
        }
    }

    IEnumerator delayRestart()
    {
        wheelHub.SetActive(false);
        if (CompareTag("Player"))
        {
            cameraRot = 0;
            //DOTween.To(() => Camera.main.fieldOfView, x => Camera.main.fieldOfView = x, 90, 1);
        }
        smokeEffect.Stop();
        trail.SetActive(false);
        if (CompareTag("Player"))
        {
            yield return new WaitForSeconds(1);
            SceneManager.LoadScene(0);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Wall") && isStartGame)
        {
            if(CompareTag("Enemy") && !isUnsteady)
            {
                //var point = other.contacts[0].point;
                var pushDir = /*point - */transform.position;
                var angle = Vector3.Angle(pushDir, transform.right);
                if (angle < 90)
                {
                    botH = -1;
                }
                else
                {
                    botH = 1;
                }
                return;
            }
            if(CompareTag("Player") && !isStunt)
            {
                return;
            }

            if (CompareTag("Player"))
            {
                GetComponent<BoxCollider>().isTrigger = true;
                isStunt = false;
                //cameraSpeed = 0;
                //cameraRot = 0;
            }
            //rigid.velocity = Vector3.zero;
            //rigid.isKinematic = true;
            anim.Play("TeTruoc");
            if (!UnityEngine.iOS.Device.generation.ToString().Contains("5"))
            {
                MMVibrationManager.Haptic(HapticTypes.LightImpact);
            }
            isStartGame = false;
            StartCoroutine(delayRestart());
        }

        if((other.transform.CompareTag("Enemy") && isStartGame && CompareTag("Player")) /*|| (other.transform.CompareTag("Enemy") && isStartGame && CompareTag("Enemy"))*/)
        {
            if (other.transform.GetComponent<Movement>().isControl || other.transform.GetComponent<Movement>().isStunt)
            {
                //Debug.LogError("Push!");
                other.transform.GetComponent<Movement>().isStunt = false;
                other.transform.GetComponent<Movement>().isControl = false;
                if (!UnityEngine.iOS.Device.generation.ToString().Contains("5") && CompareTag("Player"))
                {
                    MMVibrationManager.Haptic(HapticTypes.LightImpact);
                }
                //float collisionForce = other.impulse.magnitude / Time.fixedDeltaTime;
                var pushDir = other.transform.position - transform.position;
                other.transform.GetComponent<Movement>().rigid.AddForce(-pushDir * 40);
                var angleF = Vector3.Angle(pushDir, other.transform.forward);
                //Debug.LogError("Push!");
                if (angleF < 30)
                {
                    other.transform.GetComponent<Movement>().anim.Play("TeTruoc");
                    if (transform.CompareTag("Enemy"))
                    {
                        anim.Play("TeTruoc");
                        //Destroy(gameObject, 1.5f);
                        StartCoroutine(delayReset());
                    }
                    else
                        anim.CrossFade("LaoDaoL", 0.05f);
                }
                else
                {
                    //DOTween.To(() => other.transform.GetComponent<Movement>().rigid.velocity, x => other.transform.GetComponent<Movement>().rigid.velocity = x, Vector3.zero, 1.5f);
                    var angle = Vector3.Angle(pushDir, other.transform.right);
                    if (angle < 90)
                    {
                        other.transform.GetComponent<Movement>().anim.Play("TePhai");
                        if(transform.CompareTag("Enemy"))
                        {
                            anim.Play("TeTrai");
                            //Destroy(gameObject, 1.5f);
                            StartCoroutine(delayReset());
                        }
                        else
                            anim.CrossFade("AttackR", 0.05f);
                    }
                    else
                    {
                        other.transform.GetComponent<Movement>().anim.Play("TeTrai");
                        if (transform.CompareTag("Enemy"))
                        {
                            anim.Play("TePhai");
                            //Destroy(gameObject, 1.5f);
                            StartCoroutine(delayReset());
                        }
                        else
                            anim.CrossFade("AttackL", 0.05f);
                    }
                }
                other.transform.GetComponent<Movement>().isStartGame = false;
                other.transform.GetComponent<Movement>().PlayReset();
                //Destroy(other.gameObject, 5f);
                StartCoroutine(delayAnim(1));
            }
        }

        if (other.transform.CompareTag("Obstacle") && isStartGame && isControl)
        {
            isControl = false;
            if (CompareTag("Player"))
            {
                cameraSpeed = 0;
            }
            GetComponent<BoxCollider>().isTrigger = true;
            rigid.velocity = Vector3.zero;
            rigid.isKinematic = true;
            anim.Play("TeSau");
            if (!UnityEngine.iOS.Device.generation.ToString().Contains("5"))
            {
                MMVibrationManager.Haptic(HapticTypes.LightImpact);
            }
            StartCoroutine(delayRestart());
        }

        if (other.transform.CompareTag("Slip") && isStartGame && isControl)
        {
            isControl = false;
            if (CompareTag("Player"))
            {
                cameraSpeed = 0;
            }
            GetComponent<BoxCollider>().isTrigger = true;
            rigid.velocity = Vector3.zero;
            rigid.isKinematic = true;
            anim.Play("TeTruoc");
            if (!UnityEngine.iOS.Device.generation.ToString().Contains("5"))
            {
                MMVibrationManager.Haptic(HapticTypes.LightImpact);
            }
            StartCoroutine(delayRestart());
        }

        if (other.transform.CompareTag("Fence") && isStartGame && CompareTag("Player"))
        {
            isStartGame = false;
            isStunt = false;
            other.transform.GetComponent<Rigidbody>().AddForce(transform.forward * 10);
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("RushL1") || anim.GetCurrentAnimatorStateInfo(0).IsName("RushL2"))
            {
                GetComponent<BoxCollider>().isTrigger = true;
                //cameraSpeed = 0;
                //cameraRot = 0;
                rigid.velocity = Vector3.zero;
                rigid.isKinematic = true;
                anim.Play("DriftFailL");
                if (!UnityEngine.iOS.Device.generation.ToString().Contains("5"))
                {
                    MMVibrationManager.Haptic(HapticTypes.LightImpact);
                }
                StartCoroutine(delayReset());
            }
            else if (anim.GetCurrentAnimatorStateInfo(0).IsName("RushR1") || anim.GetCurrentAnimatorStateInfo(0).IsName("RushR2"))
            {
                GetComponent<BoxCollider>().isTrigger = true;
                //cameraSpeed = 0;
                cameraRot = 0;
                rigid.velocity = Vector3.zero;
                rigid.isKinematic = true;
                anim.Play("DriftFailR");
                if (!UnityEngine.iOS.Device.generation.ToString().Contains("5"))
                {
                    MMVibrationManager.Haptic(HapticTypes.LightImpact);
                }
                StartCoroutine(delayReset());
            }
            else
            {
                GetComponent<BoxCollider>().isTrigger = true;
                //cameraSpeed = 0;
                cameraRot = 0;
                rigid.velocity = Vector3.zero;
                rigid.isKinematic = true;
                anim.Play("TeTruoc");
                if (!UnityEngine.iOS.Device.generation.ToString().Contains("5"))
                {
                    MMVibrationManager.Haptic(HapticTypes.LightImpact);
                }
                //isStartGame = false;
                //StartCoroutine(delayRestart());
                StartCoroutine(delayReset());
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Stunt") && isStartGame)
        {
            isStunt = true;
            rotationAmount = other.transform.eulerAngles;
            if (CompareTag("Player"))
            {
                cameraRot = 0.5f;
                DOTween.To(() => cameraRot, x => cameraRot = x, 1, 1);
                DOTween.To(() => cameraSpeed, x => cameraSpeed = x, 0, 1);
                currentTurn = other.transform;
            }
            if (CompareTag("Enemy"))
            {
                StopCoroutine(botAI);
                StartCoroutine(botTurn(other.gameObject));
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Boost") && isStartGame)
        {
            rigid.AddForce(transform.forward * 50);
            int randomSkill = Random.Range(1, 4);
            anim.Play("Skill" + randomSkill.ToString());
            StartCoroutine(delayAnim(1.5f));
            if(CompareTag("Player"))
            {
                if (!UnityEngine.iOS.Device.generation.ToString().Contains("5"))
                {
                    MMVibrationManager.Haptic(HapticTypes.LightImpact);
                }
            }
        }

        if (other.CompareTag("Finish") && isStartGame)
        {
            if (CompareTag("Player"))
            {
                if (!UnityEngine.iOS.Device.generation.ToString().Contains("5"))
                {
                    MMVibrationManager.Haptic(HapticTypes.LightImpact);
                }
                rigid.AddForce(transform.up * 40);
                DOTween.To(() => rigid.velocity, x => rigid.velocity = x, transform.forward * speed, 1f);
                StartCoroutine(Finish());
            }
            else
            {
                Destroy(gameObject);
            }
        }

        if (other.CompareTag("Stunt") && isStartGame && CompareTag("Player"))
        {
            if (isControl && !isDrag && isStunt)
            {
                isControl = false;
                var time = 0.4f;
                float angle = Quaternion.Angle(lastQua, other.transform.rotation);
                if (angle > 90)
                {
                    time = angle / 90 * 0.4f + 0.4f;
                }
                var random = Random.Range(1, 2);
                if (other.transform.eulerAngles.y > transform.eulerAngles.y)
                {
                    anim.CrossFade("RushR" + random.ToString(), 0.05f);
                }
                else
                {
                    anim.CrossFade("RushL" + random.ToString(), 0.05f);
                }
                transform.DOKill();
                Time.timeScale = 0.9f;
                Time.fixedDeltaTime = 0.02F * Time.timeScale;
                transform.DORotate(lastRot, 0.2f).OnComplete(() =>
                {
                    transform.DORotate(rotationAmount, time);
                });
                lastRot = rotationAmount;
                lastQua = other.transform.rotation;
                //Debug.LogError(lastRot + " " + lastQua);
                skateBoard.transform.DOKill();
                var rotMod = rotationAmount;
                if (rotMod.y == 0)
                {
                    rotMod = new Vector3(rotMod.x, -90, rotMod.z);
                }
                skateBoard.transform.DOLocalRotate(rotMod, time / 2).OnComplete(() =>
                {
                    skateBoard.transform.DOLocalRotate(Vector3.zero, time / 1.5f);
                });
                smokeEffect.Play();
                smokeEffect.GetComponentInChildren<ParticleSystem>().Play();
                trail.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Stunt") && isStartGame)
        {
            if(CompareTag("Player"))
            {
                Time.timeScale = 1;
                Time.fixedDeltaTime = 0.02F;
                speed = originSpeed;
                //DOTween.To(() => Camera.main.fieldOfView, x => Camera.main.fieldOfView = x, 60, 2);
                DOTween.To(() => cameraSpeed, x => cameraSpeed = x, 12, 2);
                cameraRot = 1.5f;
                isStunt = false;
            }
            if (CompareTag("Enemy"))
            {
                botAI = StartCoroutine(botDecision());
                speed = originSpeed;
            }
            smokeEffect.Stop();
            smokeEffect.GetComponentInChildren<ParticleSystem>().Stop();
            trail.SetActive(false);
            StartCoroutine(delayBack());
        }
    }

    IEnumerator delayBack()
    {
        anim.CrossFade("Start", 0.05f);
        yield return new WaitForSeconds(1);
        isControl = true;
    }
}
